<div class="col-lg-6">
  <div class="product-slick">
      {!! $slickSlides[0] !!}
  </div>
  <div class="row">
      <div class="col-12 p-0">
          <div class="slider-nav">
              {!! $slickSlides[1] !!}
          </div>
      </div>
  </div>
</div>