<div class="col-lg-1 col-sm-2 col-xs-12">
  <div class="row">
      <div class="col-12 p-0">
          <div class="slider-right-nav">
              {!! $slickSlides[1] !!}
          </div>
      </div>
  </div>
</div>
<div class="col-lg-5 col-sm-10 col-xs-12 order-up">
  <div class="product-right-slick">
      {!! $slickSlides[0] !!}
  </div>
</div>