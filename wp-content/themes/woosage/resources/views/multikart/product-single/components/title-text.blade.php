<div class="border-product">
  <h6 class="product-title">{{ $title }}</h6>
  <p>{{ $text }}</p>
</div>