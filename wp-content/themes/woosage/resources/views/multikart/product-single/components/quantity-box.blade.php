<div class="qty-box">
  <div class="input-group"><span class="input-group-prepend"><button type="button"
              class="btn quantity-left-minus" data-type="minus" data-field=""><i
                  class="ti-angle-left"></i></button> </span>
      <input type="text" name="quantity" class="form-control input-number" value="1">
      <span class="input-group-prepend"><button type="button"
              class="btn quantity-right-plus" data-type="plus" data-field=""><i
                  class="ti-angle-right"></i></button></span></div>
</div>