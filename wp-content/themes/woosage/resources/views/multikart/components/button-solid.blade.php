<a href="{{ $href }}" class="btn btn-solid"
@if($modal) data-toggle="{{ $modal['toggle'] }}" data-target="{{ $modal['target'] }}" @endif>
{{ $text }}
</a> 